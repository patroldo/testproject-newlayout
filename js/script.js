$(document).ready(function() {

    $(".about-us ul li").click(function(event) {
        $(this).children("p").slideToggle();
        $(this).children(".li-header").children("i").toggleClass("fa-angle-right").toggleClass("fa-angle-down");
    });
	
});